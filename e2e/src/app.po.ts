import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(page) {
    return browser.get(page) as Promise<any>;
  }

  returnLoginNotice() {
    return element(by.css('app-root p')).getText();
  }
}
