import { AppPage } from './app.po';
import { browser, logging, element, by } from 'protractor';

describe('ChatApp Authentication', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Authenticate User and Retrieve Login Notice', () => {
    page.navigateTo('/login');
    browser.waitForAngularEnabled(false);
    //Access and insert login input fields
    element(by.id('username')).sendKeys('super');
    element(by.id('password')).sendKeys('123');
    element(by.id('login')).click();
    //Wait for server to authenticate
    browser.driver.sleep(1000);
    //Accept the alert notification
    browser.driver.switchTo().alert().accept();
    //Retrieve the login notice
    expect(page.returnLoginNotice()).toEqual('Logged in as super');
  });

  /*it('Create a user that already exists', () => {
    page.navigateTo('/manage-users');
    //browser.waitForAngularEnabled(false);
    //Access input fields to create user
    let alert = element(by.id('fail'));
    element(by.id('username')).sendKeys('super');;
    element(by.id('email')).sendKeys('super@super.com');;
    element(by.id('password')).sendKeys('123');;
    element(by.id('create')).click();
    //Wait for server to respond
    browser.driver.sleep(1000);

    //Retrieve the login notice
    expect(alert).toEqual('User: super already exists');
  });*/

});
