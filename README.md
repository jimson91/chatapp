# chatApp

## Description
A web-based chat applicaton built with the <a href="https://en.wikipedia.org/wiki/MEAN_(solution_stack)">MEAN</a> Stack: <a href="https://en.wikipedia.org/wiki/MongoDB">MongoDB</a>, <a href="https://en.wikipedia.org/wiki/Express.js">Express.js</a>, <a href="https://en.wikipedia.org/wiki/Angular_(web_framework)">Angular</a> and <a href="https://en.wikipedia.org/wiki/Node.js">Node.js</a>
<br>

ChatApp currently supports the following features:
* Instant messaging
* Managing user profile settings
* Create and manage custom groups
* Create and manage custom channels
* Administrator accounts
* Create and delete user accounts
* Token-based authentication

Support for various other features are still in development.
<br>

## Project Structure
The <a href="https://gitlab.com/jimson91/chatapp/-/tree/master/src">`/src`</a> directory contains the source code for the chat client

The <a href="https://gitlab.com/jimson91/chatapp/-/tree/master/server">`/server`</a> directory contains source code for the RESTFul API server
<br>

## Run Project
Download Dependencies

```
npm install
```

Start the application

```
npm start
```

<strong>Please Note: Project still in development</strong>