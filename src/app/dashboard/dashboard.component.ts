import { Component, OnInit, ViewChild, AfterViewChecked, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';
import { SocketService } from '../services/socket.service';
import { NotificationService } from '../services/notification.service';
import { Message } from '../models/message';
import { Title } from "@angular/platform-browser";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewChecked {
  @ViewChild('chatScroll') private chatScrollContainer: ElementRef;

  private userID: string;
  private username: string;
  private avatar: string;
  private messageForm: FormGroup;
  private messages: any[];
  private groups: [];
  private channels: [];
  private memberList: [];
  private channelNotice: string;
  private isJoined: boolean;
  private membercount: number;
  private selectedGroup: string;
  private selectedChannel: string;
  private selectedChannelID: string;
  private selectedGroupIndex: number;
  private selectedChannelIndex: number;

  constructor(
    private __TitleService: Title,
    private __AuthService: AuthService,
    private __DataService: DataService,
    private __SocketService: SocketService,
    private __NotificationService: NotificationService
  ) {
    this.__TitleService.setTitle("ChatApp | Dashboard");
    this.userID = this.__AuthService.getUserDetails('_id');
    this.username = this.__AuthService.getUserDetails('username');
    this.avatar = this.__AuthService.getUserDetails('avatar');
    this.isJoined = false;
    this.groups = [];
    this.channels = [];
    this.messages = [];
    this.memberList = [];
    this.messageForm = new FormGroup({
      message: new FormControl('', [
        Validators.required,
        Validators.maxLength(2000)
      ])
    });
  }

  get message(): any { return this.messageForm.get('message'); }

  ngOnInit() {
    this.getUserGroupList();
    this.__SocketService.establishConnection();
    this.__SocketService.joinedChannel()
      .subscribe((data: any) => {
        if (data) {
          this.isJoined = true;
        }
        else {
          this.isJoined = false;
        }
      });
    this.__SocketService.getChatHistory()
      .subscribe((data: any) => {
        this.messages = data.messages
      });
    this.__SocketService.getChannelStatus()
      .subscribe((response: any) => {
        this.channelNotice = response.notice,
          this.membercount = response.count,
          this.memberList = response.list
      });
    this.__SocketService.getChatMessage()
      .subscribe((response: any) => {
        this.messages.push({
          avatar: response.avatar,
          from: response.username,
          body: response.message,
          time: response.timestamp
        });
      });
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  ngOnDestroy() {
    const data = {
      username: this.username,
      channel_id: this.selectedChannelID
    }
    this.__SocketService.killConnection(data);
  }

  private scrollToBottom(): void {
    try {
      this.chatScrollContainer.nativeElement.scrollTop = this.chatScrollContainer.nativeElement.scrollHeight;
    } catch (err) {
    }
  }

  private getUserGroupList(): void {
    this.__DataService.requestGroupList().subscribe((groupdata: any) => {
      this.groups = groupdata;
    });
  }

  private markChannel(channel: string, index: number, id: string): void {
    this.selectedChannel = channel;
    this.selectedChannelIndex = index;
    this.selectedChannelID = id;
  }

  private joinChannel(gname: string, gid: string, cname: string, cid: string): void {
    this.selectedGroup = gname;
    this.selectedChannel = cname;
    this.selectedChannelID = cid;

    const data = {
      user_id: this.userID,
      username: this.username,
      avatar: this.avatar,
      group_id: gid,
      channel: cname,
      channel_id: cid
    }

    this.__SocketService.joinChannel(data);
    this.isJoined = true;
  }

  private leaveChannel(): void {

    const data = {
      username: this.username,
      channel_id: this.selectedChannelID
    }

    this.__SocketService.leaveChannel(data);
    this.isJoined = false;
  }

  private chat(): void {
    if (this.messageForm.valid) {

      let newMessage = new Message(
        this.selectedChannelID,
        this.userID,
        this.username,
        this.avatar,
        this.message.value);

      this.__SocketService.sendMessage(newMessage);
      this.messageForm.reset();
    }
  }
}