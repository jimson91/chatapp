import { Component, ViewChild } from '@angular/core';
import { SidenavService } from './services/sidenav.service';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild('sidenav') public sidenav: MatSidenav;

  constructor(
    private __SidenavService: SidenavService) { }

  ngAfterViewInit(): void {
    this.__SidenavService.setSidenav(this.sidenav);
  }
}
