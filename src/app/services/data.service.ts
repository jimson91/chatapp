import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  
  private SERVER_URL: string;

  constructor(private http: HttpClient) { 
    this.SERVER_URL = window.location.origin + '/api/';
  }

  public requestUserList(): Observable<any> {
    return this.http.get<any>(this.SERVER_URL + 'user/list');
  }

  public requestAdminGroupList(): Observable<any> {
    return this.http.get<any>(this.SERVER_URL + 'group/adminlist');
  }

  public requestGroupList(): Observable<any> {
    return this.http.get<any>(this.SERVER_URL + 'group/list');
  }

  public requestNewGroup(request: any): Observable<any> {
    return this.http.post<any>(this.SERVER_URL + 'group/create', request);
  }

  public requestNewMember(request: any): Observable<any> {
    return this.http.post<any>(this.SERVER_URL + 'member/addmember', request);
  }

  public requestMemberDeletion(request: any): Observable<any> {
    return this.http.post<any>(this.SERVER_URL + 'member/removemember', request);
  }

  public requestGroupDeletion(request: any): Observable<any> {
    return this.http.post<any>(this.SERVER_URL + 'group/delete', request);
  }

  public requestNewAccount(userInstance: User): Observable<any> {
    return this.http.post<any>(this.SERVER_URL + 'user/create', userInstance);
  }

  public requestAccountDeletion(request: any): Observable<any> {
    return this.http.post<any>(this.SERVER_URL + 'user/delete', request);
  }
  public requestImageUpload(formdata: any): Observable<any> {
    return this.http.post<any>(this.SERVER_URL + 'image/upload', formdata);
  }

  public requestCreateChannel(request: any): Observable<any> {
    return this.http.post<any>(this.SERVER_URL + 'channel/create', request);
  }

  public requestChannelDeletion(request: any): Observable<any> {
    return this.http.post<any>(this.SERVER_URL + 'channel/delete', request);
  }
}