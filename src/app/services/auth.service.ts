import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Role } from '../models/role';

const SERVER_URL = window.location.origin + '/api/auth';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  
  private SERVER_URL: string;
  private roles: BehaviorSubject<Role>;

  constructor(
    private router: Router, 
    private http: HttpClient, 
    public jwtHelper: JwtHelperService
    ) {
    this.SERVER_URL = window.location.origin + '/api/auth';
    this.roles = new BehaviorSubject<Role>({
      SuperAdmin: false,
      GroupAdmin: false
    });
  }

  public getPermissionStatus(): Observable<Role> {
    return this.roles.asObservable();
  }

  private updatePermissionStatus(SUstatus: boolean, GRstatus: boolean): void {
    this.roles.next({
      SuperAdmin: SUstatus,
      GroupAdmin: GRstatus
    });
  }

  public logIn(credientials: any): Observable<any> {
    return this.http.post(this.SERVER_URL + '/login', credientials);
  }

  public logOut(): void {
    localStorage.clear()
    this.updatePermissionStatus(false, false);
    this.router.navigateByUrl('login');
  }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('access_token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  public getUserDetails(attribute: string): any {
    return localStorage.getItem(attribute);
  }

  public storeSessionDetails(response: any): void {
    localStorage.setItem('_id', response._id);
    localStorage.setItem('username', response.username);
    localStorage.setItem('email', response.email);
    localStorage.setItem('avatar', response.avatar);
    localStorage.setItem('access_token', response.access_token);
  }

  public requestPermissions(): void {
    const token = localStorage.getItem('access_token');
    if (this.isAuthenticated()) {
      const tokenPayload = this.jwtHelper.decodeToken(token);
      const roles = tokenPayload.credentials.roles;
      this.updatePermissionStatus(roles.SuperAdmin, roles.GroupAdmin);
    }
  }

  public requestPasswordChange(credientials: any): Observable<any> {
    return this.http.post(this.SERVER_URL + '/authchange', credientials);
  }
}
