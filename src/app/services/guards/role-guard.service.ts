import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService implements CanActivate {

  constructor(
    public __AuthService: AuthService,
    public jwtHelper: JwtHelperService,
    public router: Router
  ) { }

  public canActivate(): boolean {
    const token = localStorage.getItem('access_token');
    const tokenPayload = this.jwtHelper.decodeToken(token);
    const roles = tokenPayload.credentials.roles;

    if (!roles.SuperAdmin && !roles.GroupAdmin) {
      this.router.navigateByUrl('dashboard');
      return false;
    }
    return true;
  }
}
