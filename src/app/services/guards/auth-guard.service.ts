import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(public __AuthService: AuthService) { }

  public canActivate(): boolean {
    if (!this.__AuthService.isAuthenticated()) {
      this.__AuthService.logOut();
      return false;
    }
    return true;
  }
}
