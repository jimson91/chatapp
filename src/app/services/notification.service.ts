import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarRef } from '@angular/material/snack-bar';
import { SnackbarComponent } from '../snackbar/snackbar.component';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private configSuccess: MatSnackBarConfig;
  private configFailure: MatSnackBarConfig;

  constructor(public snackBar: MatSnackBar) {
    this.configSuccess = {
      panelClass: 'snackbar-success',
      duration: 4000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    };
    this.configFailure = {
      panelClass: 'snackbar-error',
      duration: 4000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    };
  }

  public openSnackBar(message: string, type: string, success: boolean = true): MatSnackBarRef<SnackbarComponent> {
    if (success) {
      return this.snackBar.openFromComponent(SnackbarComponent, {
        data: {
          message: message,
          type: type
        },
        ...this.configSuccess
      });
    }
    else {
      return this.snackBar.openFromComponent(SnackbarComponent, {
        data: {
          message: message,
          type: type
        },
        ...this.configFailure
      });
    }
  }
}