import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../dialogs/confirm-dialog/confirm-dialog.component';
import { ChannelConfigDialogComponent } from '../dialogs/channel-config-dialog/channel-config-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(public dialog: MatDialog) { }

  public openConfirmDialog(msg: string) {
    return this.dialog.open(ConfirmDialogComponent, {
      width: '40%',
      disableClose: true,
      data: {
        message: msg
      }
    });
  }

  public openChannelConfiguDialog(id: string, name: string, channels: any) {
    return this.dialog.open(ChannelConfigDialogComponent, {
      width: '30%',
      disableClose: true,
      data: {
        group: {
          id: id,
          name: name,
        },
        channels: channels
      }
    });
  }
}
