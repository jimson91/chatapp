import * as io from 'socket.io-client';
import { Injectable } from '@angular/core';
import { Message } from '../models/message';
import { Observable, Observer } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  
  private socket: io;
  private SERVER_URL: string;

  constructor() {
    this.SERVER_URL = window.location.origin + '/chat';
  }

  public establishConnection(): void {
    if (!this.socket) {
      this.socket = io(this.SERVER_URL);
    }
  }

  public sendMessage(message: Message): void {
    this.socket.emit('message', message);
  }

  public joinChannel(data: any): void {
    this.socket.emit("join_channel", data);
  }

  public leaveChannel(data: any): void {
    this.socket.emit("leave_channel", data);
  }

  public getChatHistory(): Observable<any> {
    return Observable.create((observer: Observer<any>) => {
      this.socket.on('history', (data: any) => {
        observer.next(data);
      });
    });
  }

  public getChatMessage(): Observable<any> {
    return Observable.create((observer: Observer<any>) => {
      this.socket.on('message', (response: any) => {
        observer.next(response);
      });
    });
  }

  public joinedChannel(): Observable<any> {
    return Observable.create((observer: Observer<any>) => {
      this.socket.on('joined', (response: any) => {
        observer.next(response);
      });
    });
  }

  public getChannelStatus(): Observable<any> {
    return Observable.create((observer: Observer<any>) => {
      this.socket.on('status', (response: any) => {
        observer.next(response);
      });
    });
  }

  public killConnection(data: any): void {
    if (this.socket) {
      this.leaveChannel(data);
      this.socket.removeAllListeners();
      this.socket.close();
      this.socket = undefined;
    }
  }
}
