import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

@Injectable({
  providedIn: 'root'
})
export class SidenavService {
  private sidenav: MatSidenav;
  public opened: boolean;

  constructor() {
    this.opened = true;
  }

  public setSidenav(sidenav: MatSidenav): void {
    this.sidenav = sidenav;
    this.opened = this.sidenav.opened;
  }

  public toggleSideNav(): void {
    this.sidenav.toggle();
    this.opened = this.sidenav.opened;
  }
}
