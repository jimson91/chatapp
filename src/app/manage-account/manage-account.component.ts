import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidationErrors } from '@angular/forms';
import { Title } from "@angular/platform-browser";
import { NotificationService } from '../services/notification.service';
import { DataService } from '../services/data.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-manage-account',
  templateUrl: './manage-account.component.html',
  styleUrls: ['./manage-account.component.scss']
})
export class ManageAccountComponent {
  
  private avatar: string;
  private errormsg: string;
  private selectedImage: any;
  private passwordForm: FormGroup;
  private hideCurrentPassword: boolean;
  private hideNewPassword: boolean;

  constructor(
    private __TitleService: Title,
    private __NotificationService: NotificationService,
    private __DataService: DataService,
    private __AuthService: AuthService
  ) {

    this.hideCurrentPassword = true;
    this.hideNewPassword = true;

    this.passwordForm = new FormGroup({
      currentPassword: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      newPassword: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      newPasswordConfirm: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        this.passwordsMatchValidator
      ]),
    });
    this.__TitleService.setTitle("ChatApp | Account Management");
  }

  get currentPassword(): any { return this.passwordForm.get('currentPassword'); }
  get newPassword(): any { return this.passwordForm.get('newPassword'); }
  get newPasswordConfirm(): any { return this.passwordForm.get('newPasswordConfirm'); }

  private passwordsMatchValidator(control: FormControl): ValidationErrors {
    let password = control.root.get('newPassword');
    return password && control.value !== password.value ? {
      passwordMatch: true
    } : null;
  }

  private onFileSelected(event: any) {
    this.selectedImage = event.target.files[0];
  }

  private onUpload() {
    const formdata = new FormData();
    formdata.append('image', this.selectedImage, this.selectedImage.name);
    this.__DataService.requestImageUpload(formdata).subscribe(response => {
      localStorage.setItem('avatar', response.data.filename);
      this.avatar = response.data.filename;
    });
  }

  private changePassword() {
    if (this.passwordForm.valid) {
      this.__AuthService.requestPasswordChange(this.passwordForm.getRawValue()).subscribe(response => {
        if (response.valid) {
          this.__NotificationService.openSnackBar(response.message, 'verified', true);
        }
        else {
          this.errormsg = response.message;
        }
      });
    }
  }
}
