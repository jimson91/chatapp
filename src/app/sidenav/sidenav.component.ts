import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Role } from '../models/role';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent {
  private roles: Role;
  private active: string;

  constructor(private __AuthService: AuthService) {
    if (__AuthService.isAuthenticated()) {
      __AuthService.requestPermissions();
    }
    __AuthService.getPermissionStatus().subscribe(roles => {
      this.roles = roles;
    });
  }
}
