import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { SidenavService } from '../services/sidenav.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  constructor(
    private __AuthService: AuthService, 
    private __SidenavService: SidenavService
    ) { }

  public toggleSideNav(): void {
    this.__SidenavService.toggleSideNav();
  }
}
