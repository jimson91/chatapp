import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { DataService } from '../services/data.service';
import { AuthService } from '../services/auth.service';
import { DialogService } from '../services/dialog.service';
import { NotificationService } from '../services/notification.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Role } from '../models/role';

@Component({
  selector: 'app-manage-groups',
  templateUrl: './manage-groups.component.html',
  styleUrls: ['./manage-groups.component.scss']
})
export class ManageGroupsComponent implements OnInit {
  
  private roles: Role;
  private groupForm: FormGroup;
  private users: [];
  private members: string[];
  private channels: string[];
  private adminGroups: [];
  private selectedUser: string;
  private selectedGroup: string;
  private selectedMember: string;
  private selectedChannel: string;
  private selectedUserID: string;
  private selectedGroupID: string;
  private selectedChannelID: string;
  private selectedMemberID: string;
  private selectedUserIndex: number;
  private selectedGroupIndex: number;
  private selectedChannelIndex: number;
  private selectedMemberIndex: number;

  constructor(
    private __TitleService: Title,
    private __DataService: DataService,
    private __AuthService: AuthService,
    private __DialogService: DialogService,
    private __NotificationService: NotificationService
  ) {
    this.__TitleService.setTitle("ChatApp | Group Administration");
    
    this.users = [];
    this.members = [];
    this.channels = [];
    this.adminGroups = [];

    this.groupForm = new FormGroup({
      newGroupName: new FormControl('', [
        Validators.required,
        Validators.maxLength(20)
      ]),
    });
  }

  get newGroupName(): any { return this.groupForm.get('newGroupName'); }

  ngOnInit() {
    this.__AuthService.getPermissionStatus().subscribe(roles => {
      this.roles = roles;
      if (roles.SuperAdmin || roles.GroupAdmin) {
        this.getGlobalUserList();
        this.getGroupList();
      }
    });
  }

  private getGlobalUserList() {
    this.__DataService.requestUserList().subscribe((userlist: any) => {
      this.users = userlist;
    });
  }

  private getGroupList() {
    this.__DataService.requestAdminGroupList().subscribe((grouplist: any) => {
      this.adminGroups = grouplist
    })
  }

  private addMember(username: string, index: number, id: string) {
    if (username && this.selectedGroup) {
      let query = {
        userid: id,
        username: username,
        groupid: this.selectedGroupID,
        groupname: this.selectedGroup
      };
      this.__DataService.requestNewMember(query).subscribe((response: any) => {
        if (response.valid) {
          this.__NotificationService.openSnackBar(response.message, 'verified');
          this.members.push(this.users[index]);
          this.selectedUserIndex = null;
        }
        else {
          this.__NotificationService.openSnackBar(response.message, 'error', false);
        }
      });
    }
    else {
      this.__NotificationService.openSnackBar("Select a User and Group", 'error', false);
    }
  }

  private removeMember(username: string, index: number, id: string) {
    if (username && this.selectedGroup) {
      let query = {
        memberid: id,
        groupid: this.selectedGroupID,
        membername: username,
        groupname: this.selectedGroup
      }
      this.__DataService.requestMemberDeletion(query).subscribe((response: any) => {
        if (response.valid) {
          this.__NotificationService.openSnackBar(response.message, 'verified');
          this.members.splice(index, 1);
          this.selectedMember = "";
          this.selectedMemberIndex = null;
        }
        else {
          this.__NotificationService.openSnackBar(response.message, 'error', false);
        }
      });
    }
    else {
      this.__NotificationService.openSnackBar("Select a Group and Member", 'error', false);
    }
  }

  private createGroup() {
    if (this.groupForm.valid) {
      this.__DataService.requestNewGroup(this.groupForm.getRawValue()).subscribe((response: any) => {
        if (response.valid) {
          this.__NotificationService.openSnackBar(response.message, 'verified');
          this.getGroupList();
        }
        else {
          this.__NotificationService.openSnackBar(response.message, 'error', false);
        }
      });
    }
  }

  private channelConfiguration(id: string, name: string, channels: any) {
    this.__DialogService.openChannelConfiguDialog(id, name, channels).afterClosed().subscribe();
  }

  private deleteGroup(name: string, id: string) {
    this.__DialogService.openConfirmDialog("Are you sure you want to delete " + name).afterClosed().subscribe(confirm => {
      if (name && id && confirm) {
        let query = { group_id: id, group: name };
        this.__DataService.requestGroupDeletion(query).subscribe((response: any) => {
          if (response.valid) {
            this.__NotificationService.openSnackBar(response.message, 'verified');
            this.selectedGroup = "";
            this.getGroupList();
            this.members = [];
          }
          else {
            this.__NotificationService.openSnackBar(response.message, 'error', false);
          }
        });
      }
    });
  }

  private markUser(user: string, index: number, id: string) {
    this.selectedUser = user;
    this.selectedUserIndex = index;
    this.selectedUserID = id;
  }

  private markGroup(group: string, index: number, id: string, members: string[], channels: string[]) {
    this.selectedGroup = group;
    this.selectedGroupIndex = index;
    this.selectedGroupID = id;
    this.members = members;
    this.channels = channels;
  }

  private markMember(member: string, index: number, id: string) {
    this.selectedMember = member;
    this.selectedMemberIndex = index;
    this.selectedMemberID = id;
  }
}
