import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataService } from '../../services/data.service';
import { NotificationService } from '../../services/notification.service';

@Component({
  selector: 'app-channel-config-dialog',
  templateUrl: './channel-config-dialog.component.html',
  styleUrls: ['./channel-config-dialog.component.scss']
})
export class ChannelConfigDialogComponent {

  private channels: any[];
  private channelForm: FormGroup;
  private selectedGroupID: string;
  private selectedChannel: string;
  private selectedChannelID: string;
  private selectedChannelIndex: number;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data, 
    public __DialogRef: MatDialogRef<ChannelConfigDialogComponent>,
    private __DataService: DataService,
    private __NotificationService: NotificationService
  ) {
    this.selectedGroupID = data.group.id;
    this.channels = data.channels;

    this.channelForm = new FormGroup({
      newChannelName: new FormControl('', [
        Validators.required,
        Validators.maxLength(20)
      ])
    });
  }

  get newChannelName(): any { return this.channelForm.get('newChannelName'); }

  private markChannel(channel: string, index: number, id: string) {
    this.selectedChannel = channel;
    this.selectedChannelIndex = index;
    this.selectedChannelID = id;
  }

  private createChannel() {
    if (this.channelForm.valid) {
      let channel = { newchannelname: this.channelForm.getRawValue().newChannelName, group_id: this.selectedGroupID };
      this.__DataService.requestCreateChannel(channel).subscribe((response: any) => {
        if (response.valid) {
          this.__NotificationService.openSnackBar(response.message, 'verified');
        } else {
          this.__NotificationService.openSnackBar(response.message, 'error', false);
        }
      });
    }
  }

  private deleteChannel(name: string, index: number, id: string) {
    if (name && id && confirm) {
      let query = { group_id: this.selectedGroupID, channel_id: id };
      this.__DataService.requestChannelDeletion(query).subscribe((response: any) => {
        if (response.valid) {
          this.__NotificationService.openSnackBar(response.message, 'verified');
          this.channels.splice(index, 1);
        }
        else {
          this.__NotificationService.openSnackBar(response.message, 'error', false);
        }
      });
    }
  }

  private onDismiss(): void {
    this.__DialogRef.close(false);
  }
}
