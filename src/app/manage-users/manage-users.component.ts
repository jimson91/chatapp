import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { AuthService } from '../services/auth.service';
import { DialogService } from '../services/dialog.service';
import { NotificationService } from '../services/notification.service';
import { FormGroup, FormControl, Validators, ValidationErrors } from '@angular/forms';
import { Title } from "@angular/platform-browser";
import { Role } from '../models/role';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.scss']
})
export class ManageUsersComponent implements OnInit {
  
  private roles: Role;
  private registerForm: FormGroup;
  private users: string[];
  private selectedUser: string;
  private selectedUserID: string;
  private selectedUserIndex: number;

  constructor(
    private __TitleService: Title,
    private __DataService: DataService,
    private __AuthService: AuthService,
    private __DialogService: DialogService,
    private __NotificationService: NotificationService
  ) {
    this.__TitleService.setTitle("ChatApp | User Administration");
    this.users = [];
    this.registerForm = new FormGroup({
      username: new FormControl('', [
        Validators.required,
        Validators.maxLength(20)
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      confirm: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        this.passwordsMatchValidator
      ]),
      roles: new FormGroup({
        SuperAdmin: new FormControl(false, [Validators.required]),
        GroupAdmin: new FormControl(false, [Validators.required])
      })
    });
  }

  ngOnInit() {
    this.__AuthService.getPermissionStatus().subscribe(roles => {
      this.roles = roles;
      if (roles.SuperAdmin) {
        this.getGlobalUserList();
      }
    });
  }

  get username(): any { return this.registerForm.get('username'); }
  get email(): any { return this.registerForm.get('email'); }
  get password(): any { return this.registerForm.get('password'); }
  get confirm(): any { return this.registerForm.get('confirm'); }
  get SUadmin(): any { return this.registerForm.get('roles.SuperAdmin') }
  get GRadmin(): any { return this.registerForm.get('roles.GroupAdmin') }

  private passwordsMatchValidator(control: FormControl): ValidationErrors {
    let password = control.root.get('password');
    return password && control.value !== password.value ? {
      passwordMatch: true
    } : null;
  }

  private getGlobalUserList() {
    this.__DataService.requestUserList().subscribe((userlist: any) => {
      this.users = userlist;
    });
  }

  private registerUser() {
    if (this.registerForm.valid) {
      this.__DataService.requestNewAccount(this.registerForm.getRawValue()).subscribe((response: any) => {
        if (response.valid) {
          this.getGlobalUserList();
          this.__NotificationService.openSnackBar(response.message, 'verified', true);
        }
        else {
          this.__NotificationService.openSnackBar(response.message, 'error', false);
        }
      })
    }
  }

  private deleteUser(user: string, index: number, id: string) {
    this.__DialogService.openConfirmDialog("Are you sure you want to delete " + user).afterClosed().subscribe(confirm => {
      if (id && user && confirm) {
        let query = this.users[index];
        this.__DataService.requestAccountDeletion(query).subscribe((response: any) => {
          if (response.valid) {
            this.getGlobalUserList();
            this.selectedUser = "";
            this.selectedUserIndex = null;
            this.__NotificationService.openSnackBar(response.message, 'verified', true);
          }
          else {
            this.__NotificationService.openSnackBar(response.message, 'error', false);
          }
        });
      }
    });
  }

  private markUser(user: string, index: number, ID: string) {
    this.selectedUser = user;
    this.selectedUserID = ID;
    this.selectedUserIndex = index;
  }
}