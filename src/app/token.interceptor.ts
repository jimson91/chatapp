import { Injectable } from '@angular/core';
import { AuthService } from './services/auth.service';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { NotificationService } from './services/notification.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private __AuthService: AuthService, private __NotificationService: NotificationService) {

    }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = localStorage.getItem('access_token');

        if (token) {
            request = request.clone({
                setHeaders: {
                    'Authorization': `Bearer ${token}`
                }
            });

            return next.handle(request).pipe(tap(() => { }, (err: any) => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status === 401) {
                        this.__NotificationService.openSnackBar(err.error['Invalid Token'], 'error', false);
                        this.__AuthService.logOut();
                    }
                }
            }));
        }
        else {
            request = request.clone({
                setHeaders: {
                    'Content-Type': 'application/json'
                }
            });

        }
        return next.handle(request);
    }
}