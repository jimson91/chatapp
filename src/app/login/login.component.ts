import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidationErrors } from '@angular/forms';
import { Title } from "@angular/platform-browser";
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  
  private error: string;
  private authForm: FormGroup;
  private registerForm: FormGroup;

  constructor(
    private router: Router,
    private __TitleService: Title,
    private __AuthService: AuthService,
    private __DataService: DataService,
    private __NotificationService: NotificationService
  ) {
    this.__TitleService.setTitle("ChatApp | Sign In");

    this.authForm = new FormGroup({
      username: new FormControl('', [
        Validators.required,
        Validators.maxLength(20)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
    });

    this.registerForm = new FormGroup({
      newusername: new FormControl('', [
        Validators.required,
        Validators.maxLength(20)
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      newpassword: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      confirm: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        this.passwordsMatchValidator
      ]),
    });
  }

  ngOnInit() {
    if (this.__AuthService.isAuthenticated()) {
      this.router.navigateByUrl('dashboard');
    }
  }

  get username(): any { return this.authForm.get('username'); }
  get password(): any { return this.authForm.get('password'); }
  get newusername(): any { return this.registerForm.get('newusername'); }
  get email(): any { return this.registerForm.get('email'); }
  get newpassword(): any { return this.registerForm.get('newpassword'); }
  get confirm(): any { return this.registerForm.get('confirm'); }

  private passwordsMatchValidator(control: FormControl): ValidationErrors {
    let password = control.root.get('newpassword');
    return password && control.value !== password.value ? {
      passwordMatch: true
    } : null;
  }

  private login() {
    if (this.authForm.valid) {
      this.__AuthService.logIn(this.authForm.getRawValue()).subscribe((response: any) => {
        if (response.access_token) {
          this.__AuthService.storeSessionDetails(response);
          this.router.navigateByUrl('dashboard');
          this.__AuthService.requestPermissions();
        } else {
          this.error = response.message;
        }
      });
    }
  }

  private registerUser() {
    if (this.registerForm.valid) {
      this.__DataService.requestNewAccount(this.registerForm.getRawValue()).subscribe((response: any) => {
        if (response.valid) {
          this.__NotificationService.openSnackBar(response.message, 'verified');
        }
        else {
          this.__NotificationService.openSnackBar(response.message, 'error', false);
        }
      });
    }
  }
}
