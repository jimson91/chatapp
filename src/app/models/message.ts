export class Message {
    channel_id: string;
    user_id: string;
    username: string;
    avatar: string;
    message: string;

    constructor(channel_id: string, user_id: string, username: string, avatar: string, message: string) {
        this.channel_id = channel_id;
        this.user_id = user_id;
        this.username = username;
        this.avatar = avatar;
        this.message = message;
    }
}
