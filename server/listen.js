module.exports = function (http, PORT, HOST) {

  http.listen(PORT, () => {
    console.log(`Server listening on: ${HOST}:${PORT}`);
  });
}