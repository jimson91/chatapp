const jwt = require('jsonwebtoken');

const verifyToken = async (req, res, next) => {
    const token = req.header('Authorization');

    if (!token) return res.status(401).send('Unauthorised Request');

    try {
        const raw_token = token.replace("Bearer ", "");
        const auth_data = jwt.verify(raw_token, 'secretkey');
        req.verified = auth_data;
        next();
    }
    catch (err) {
        return res.status(401).send({ 'Invalid Token': err.name });
    }
}

module.exports = verifyToken;