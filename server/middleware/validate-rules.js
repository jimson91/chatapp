const { body, validationResult } = require('express-validator');

const authenticate = () => {
  return [
    body('username').notEmpty().isString().isLength({ max: 20 }),
    body('password').isString().isLength({ min: 6 })
  ]
}

const authChange = () => {
  return [
    body('currentPassword').notEmpty().isString().isLength({ min: 6 }),
    body('newPassword').notEmpty().isString().isLength({ min: 6 })
  ]
}

const userCreate = () => {
  return [
    body('username').notEmpty().isString().isLength({ max: 20 }),
    body('email').isEmail(),
    body('password').isLength({ min: 6 }),
    body('roles.SuperAdmin').isBoolean(),
    body('roles.GroupAdmin').isBoolean()
  ]
}

const userDelete = () => {
  return [
    body('_id').isMongoId(),
    body('username').notEmpty().isString().isLength({ max: 20 })
  ]
}

const memberAdd = () => {
  return [
    body('userid').isMongoId(),
    body('groupid').isMongoId(),
    body('username').notEmpty().isString().isLength({ max: 20 }),
    body('groupname').notEmpty().isString().isLength({ max: 20 })
  ]
}

const memberRemove = () => {
  return [
    body('memberid').isMongoId(),
    body('groupid').isMongoId(),
    body('membername').notEmpty().isString().isLength({ max: 20 }),
    body('groupname').notEmpty().isString().isLength({ max: 20 })
  ]
}

const groupCreate = () => {
  return [
    body('newGroupName').notEmpty().isString().isLength({ max: 20 })
  ]
}

const groupDelete = () => {
  return [
    body('group').notEmpty().isString().isLength({ max: 20 }),
    body('group_id').isMongoId()
  ]
}

const channelCreate = () => {
  return [
    body('group_id').isMongoId(),
    body('newchannelname').notEmpty().isString(),
    body('')
  ]
}

const channelDelete = () => {
  return [
    body('group_id').isMongoId(),
    body('channel_id').isMongoId()
  ]
}

const validate = (req, res, next) => {
  const errors = validationResult(req)
  if (errors.isEmpty()) {
    return next()
  }
  const extractedErrors = []
  errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }))

  return res.status(422).json({
    valid: false,
    message: extractedErrors,
  });
}

module.exports = {
  authenticate,
  authChange,
  userCreate,
  userDelete,
  groupCreate,
  groupDelete,
  channelCreate,
  channelDelete,
  memberAdd,
  memberRemove,
  validate
}