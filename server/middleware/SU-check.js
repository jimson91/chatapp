const checkSUPermissions = async (req, res, next) => {

    const permissions = req.verified.credentials.roles;

    if (permissions.SuperAdmin) {
        next();
    }
    else {
        return res.status(401).send('Unauthorised Request');
    }
}

module.exports = checkSUPermissions;