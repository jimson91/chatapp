const Bcrypt = require("bcryptjs");

module.exports = async function (db, mongoose) {
    
    const admin = new mongoose.Types.ObjectId();
    const user2 = new mongoose.Types.ObjectId();
    const user3 = new mongoose.Types.ObjectId();
    const group1 = new mongoose.Types.ObjectId();
    const group2 = new mongoose.Types.ObjectId();
    const group3 = new mongoose.Types.ObjectId();
    const channel1 = new mongoose.Types.ObjectId();
    const channel2 = new mongoose.Types.ObjectId();

    const users_data = [
        {
            "_id": admin,
            "username": "admin",
            "password": Bcrypt.hashSync("123456", 10),
            "email": "super@super.com",
            "avatar": "default.jpg",
            "roles": {
                "SuperAdmin": true,
                "GroupAdmin": true
            },
            "groups": [
                group1,
                group2
            ]
        },
        {
            "_id": user2,
            "username": "user2",
            "password": Bcrypt.hashSync("123456", 10),
            "email": "sam@sam.com",
            "avatar": "default.jpg",
            "roles": {
                "SuperAdmin": false,
                "GroupAdmin": true
            },
            "groups": [
                group1,
                group3
            ]
        },
        {
            "_id": user3,
            "username": "user3",
            "password": Bcrypt.hashSync("123456", 10),
            "email": "tom@tom.com",
            "avatar": "default.jpg",
            "roles": {
                "SuperAdmin": false,
                "GroupAdmin": false
            },
            "groups": [
                group1,
                group3,
                group2
            ]
        }
    ]

    const group_data = [
        {
            "_id": group1,
            "name": "Group1",
            "admin": admin,
            "members": [
                admin,
                user2,
                user3
            ]
        },
        {
            "_id": group2,
            "name": "Group2",
            "admin": admin,
            "members": [
                admin,
                user3
            ]
        },
        {
            "_id": group3,
            "name": "Group3",
            "admin": user2,
            "members": [
                user3,
                user2
            ]
        }
    ]

    const channel_data = [
        {
            "_id": channel1,
            "name": "Channel 1",
            "group_id": group1,
            "messages": [
                {
                    'avatar': 'default.jpg',
                    '_id': admin,
                    'from': 'admin',
                    'body': "Hello Everyone",
                    'time': 'June 14th 2020, 7:21 pm'
                },
                {
                    'avatar': 'default.jpg',
                    '_id': user2,
                    'from': 'user2',
                    'body': "Hey super, whats up?",
                    'time': 'June 14th 2020, 7:21 pm'
                }
            ]

        },
        {
            "_id": channel2,
            "name": "Channel 2",
            "group_id": group1,
            "messages": [
                {
                    'avatar': 'default.jpg',
                    '_id': admin,
                    'from': 'admin',
                    'body': "Hello Everyone",
                    'time': 'June 14th 2020, 7:21 pm'
                }
            ]
        }
    ]

    //Drop Collection (if collection exists) in case we change the dummy data structure
    try {
        await db.collection('users').drop();
        await db.collection('groups').drop();
        await db.collection('channels').drop();
    }
    finally {
        //Create the collection
        const users_collection = await db.collection('users');
        const groups_collection = await db.collection('groups');
        const channel_collection = await db.collection('channels');
        //Insert the dummy data
        const users_count = await users_collection.find().count();
        const groups_count = await groups_collection.find().count();
        const channel_count = await channel_collection.find().count();

        if (users_count == 0) {
            await users_collection.insertMany(users_data);
            console.log("Users have been seeded");
        }
        if (groups_count == 0) {
            await groups_collection.insertMany(group_data);
            console.log("Groups have been seeded");
        }
        if (channel_count == 0) {
            await channel_collection.insertMany(channel_data);
            console.log("Channels have been seeded");
        }
    }
}