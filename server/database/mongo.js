module.exports = function (app, io) {

    const mongoose = require('mongoose');

    mongoose.connect(
        'mongodb://localhost:27017/ChatApp', //Mongo database URI
        {
            poolSize: 10,
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true
        }, //Connection options
        (error, db) => { //Callback function code. When we have a connection start the rest of the app
            if (error) { return console.log(error) };

            //Seed the database with intial data
            require('./seed.js')(db, mongoose);

            //Import web sockets code
            require('../socket.js')(io);

            //Import routes
            const auth = require('../routes/auth');
            const image = require('../routes/image');
            const users = require('../routes/users');
            const groups = require('../routes/groups');
            const channels = require('../routes/channels');
            const members = require('../routes/members');
            const index = require('../routes/index');

            //Since the angular code handles routing
            app.use('/api/image', image);
            app.use('/api/auth', auth);
            app.use('/api/user', users);
            app.use('/api/group', groups);
            app.use('/api/channel', channels);
            app.use('/api/member', members);
            app.use('*', index);
        });
}
