const Channel = require('./models/Channel');
const Group = require('./models/Group');
const moment = require('moment')

module.exports = (io) => {
    var activeChannels = [];

    const chat = io.of('/chat');

    chat.on('connection', async (socket) => {
        console.log("connection established: socket ID is " + socket.id);

        //Event to send message back to the clients
        socket.on('message', async (data) => {
            channelID = data.channel_id;

            for (let i in activeChannels) {
                if (activeChannels[i].cid == data.channel_id) {
                    for (let x in activeChannels[i].members) {
                        // check each if current socket id has joined a channel
                        if (activeChannels[i].members[x].sid == socket.id) {
                            const timestamp = moment().format('MMMM Do YYYY, h:mm a');
                            try {
                                // store the message in the channels collection
                                await Channel.findByIdAndUpdate(channelID,
                                    {
                                        $push: {
                                            'messages': {
                                                'avatar': data.avatar,
                                                '_id': data.user_id,
                                                'from': data.username,
                                                'body': data.message,
                                                'time': timestamp
                                            }
                                        }
                                    });
                                // send the message back to the channel
                                chat.to(activeChannels[i].cid).emit('message', {
                                    avatar: data.avatar,
                                    username: data.username,
                                    message: data.message,
                                    timestamp: timestamp
                                });
                            }
                            catch (err) {
                                console.log(err);
                            }
                        }
                    }
                }
            }
        });

        //Event to join a channel
        socket.on('join_channel', async (data) => {
            const channelID = data.channel_id;
            const userID = data.user_id;
            const groupID = data.group_id;
            //Check if channel exists and that the user is a member before joining
            const channelExists = await Channel.countDocuments({ "_id": channelID });
            const isGroupMember = await Group.countDocuments({ '_id': groupID, 'members': userID });

            if (channelExists && isGroupMember) {
                socket.join(data.channel_id, async () => {
                    var inroom = false;
                    var active = false;
                    var index = null;
                    var count = null;
                    // check if the channel is active
                    for (let i in activeChannels) {
                        if (activeChannels[i].cid == data.channel_id) {
                            index = i;
                            active = true;
                            break;
                        }
                    }

                    if (active) {
                        // check if user is already in the channel
                        for (let x in activeChannels[index].members) {
                            if (activeChannels[index].members[x].uid == data.user_id) {
                                activeChannels[index].members[x].sid = socket.id;
                                inroom = true;
                                break;
                            }
                        }
                        // if not, append to the list active members
                        if (!inroom) {
                            activeChannels[index].count += 1;
                            activeChannels[index].members.push({
                                sid: socket.id,
                                uid: data.user_id,
                                user: data.username,
                                avatar: data.avatar
                            });
                        }
                        count = activeChannels[index].count;
                    }
                    else {
                        // if the channel is not active, create the new channel and add member
                        var socketChannel = {
                            cid: data.channel_id,
                            cname: data.channel,
                            count: 1,
                            members: [{
                                sid: socket.id,
                                uid: data.user_id,
                                user: data.username,
                                avatar: data.avatar
                            }]
                        }
                        activeChannels.push(socketChannel);
                        count = 1;
                        index = activeChannels.indexOf(socketChannel);
                    }

                    // send the list of channel messages back to the client
                    Channel.findOne({ "_id": channelID }).select({ "messages": 1 }).exec((err, messages) => {
                        if (err) throw err;
                        return chat.in(data.channel_id).emit('history', messages);
                    });
                    //Send back a notice with the username of the member who joined
                    chat.in(data.channel_id).emit('status', {
                        notice: data.username + ' has joined the discussion',
                        count: count,
                        list: activeChannels[index].members
                    });
                });
                return chat.in(data.channel_id).emit('joined', data.channel);
            }
        });
        //Event to leave a channel
        socket.on('leave_channel', async (data) => {
            var count = 0;
            var index = 0;

            for (let i in activeChannels) {
                if (activeChannels[i].cid == data.channel_id) {
                    index = i;
                    for (let x in activeChannels[i].members) {
                        if (activeChannels[i].members[x].sid == socket.id) {
                            activeChannels[i].members.splice(x, 1);
                            activeChannels[i].count -= 1;
                            count = activeChannels[i].count;
                            socket.leave(data.channel_id);
                            break;
                        }
                    }
                }
            }
            if (count != 0) {
                //Send back a notice with the username of the member who left
                chat.to(data.channel_id).emit('status', {
                    notice: data.username + ' has left the discussion',
                    count: count,
                    list: activeChannels[index].members
                });
            } else {
                // if there is are no members left in the room, close the channel
                activeChannels.splice(index, 1);
            }
        });

        //Event to disconnect from the socket
        socket.on('disconnect', async () => {
            chat.emit('disconnect');
            console.log("client disconnected");
        });
    });
}