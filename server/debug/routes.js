const assert = require('assert');
const app = require('../server.js');
const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const should = chai.should();
const SERVER_URL = process.env.HOST + ':8080/api';
console.log(SERVER_URL);
chai.use(chaiHttp);
let rootWebToken = '';

describe('Route Test Suite', function () {
  // Define test suite checks the /auth route which authenticates the user
  describe('/auth', () => {
    it('Send valid username and password', (done) => {
      chai.request(SERVER_URL)
        .post('/auth/login')
        .type('form')
        .send({
          'username': 'admin',
          'password': "123456"
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('_id');
          res.body.should.have.property('username');
          res.body.should.have.property('email');
          res.body.should.have.property('avatar');
          res.body.should.have.property('roles');
          res.body.should.have.property('access_token');
          rootWebToken = res.body.access_token;
          done();
        });
    });
    it('Send invalid username and password', (done) => {
      chai.request(SERVER_URL)
        .post('/auth/login')
        .type('form')
        .send({
          'username': '',
          'password': ''
        })
        .end((err, res) => {
          res.body.should.have.property('valid');
          done();
        });
    });
    it('Send invalid login request', (done) => {
      chai.request(SERVER_URL)
        .post('/auth/login')
        .type('form')
        .send({
          'name': '',
          'passwrd': ''
        })
        .end((err, res) => {
          assert.equal(res.body.valid, false)
          res.should.have.status(422);
          done();
        });
    });
  })

  // Define test suite checks the /userlist route which retrieves the global lsit of users
  describe('/user/list', () => {
    it('Send authorised request to get global list of users', (done) => {
      chai.request(SERVER_URL)
        .get('/user/list')
        .set({
          Authorization: `Bearer ${rootWebToken}`
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.length.should.be.eql(2);
          done();
        });
    });
    it('Send unauthorised request to get global list of users', (done) => {
      chai.request(SERVER_URL)
        .get('/user/list')
        .end((err, res) => {
          res.should.have.status(401);
          done();
        });
    });

  });

  describe('/group/list', () => {
    it('Send authorised request to get list of all groups the user belongs', (done) => {
      chai.request(SERVER_URL)
        .get('/group/list')
        .set({
          Authorization: `Bearer ${rootWebToken}`
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          done();
        });
    });
    it('Send unauthorised request to get list of groups the user controls', (done) => {
      chai.request(SERVER_URL)
        .get('/group/list')
        .end((err, res) => {
          res.should.have.status(401);
          done();
        });
    });

  });
  // Define test suite checks the admingrouplist route
  describe('/group/adminlist', () => {
    it('Send authorised request to get list of groups the user controls', (done) => {
      chai.request(SERVER_URL)
        .get('/group/adminlist')
        .set({
          Authorization: `Bearer ${rootWebToken}`
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          done();
        });
    });
    it('Send unauthorised request to get list of groups the user controls', (done) => {
      chai.request(SERVER_URL)
        .get('/group/adminlist')
        .end((err, res) => {
          res.should.have.status(401);
          done();
        });
    });

  });
  describe('/group/create', () => {
    it('it should create a new group for authenticated user', (done) => {
      chai.request(SERVER_URL)
        .post('/group/create')
        .set({
          Authorization: `Bearer ${rootWebToken}`
        })
        .send({
          newGroupName: 'newgroup'
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('valid').eql(true);
          res.body.should.have.property('message').eql('newgroup was created');
          done();
        });
    });
    it('it should fail to create new group as same group already exists', (done) => {
      chai.request(SERVER_URL)
        .post('/group/create')
        .set({
          Authorization: `Bearer ${rootWebToken}`
        })
        .send({
          newGroupName: 'newgroup'
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('valid').eql(false);
          res.body.should.have.property('message').eql('newgroup already exists');
          done();
        });
    });

  });
});
