const moment = require('moment')

// define a middleware function that logs each http request to console
const logger = (req, res, next) => {
  const timestamp = moment().format('h:mm:ss A');
  const method = req.method;
  const source = `Resource: ${req.protocol}://${req.get('host')}${req.originalUrl}`;
  const remote = 'Remote Client: ' + req.ip.substr(7);

  const colors = {
    yellow: "\x1b[33m",
    white: "\x1b[37m",
    purple: "\x1b[35m"
  }
  console.log(
    `${colors.white}${remote}` +
    `${colors.purple} [${timestamp}]` +
    `${colors.yellow} ${method}` +
    `${colors.white} ${source}`
  );
  next()
}

module.exports = logger;