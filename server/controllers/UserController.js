const User = require('../models/User');
const Group = require('../models/Group');

const create = async (req, res) => {
    try {
        //Create an instance of a user and assign the recieved values
        const result = await User.create({
            'username': req.body.username,
            'password': req.body.password,
            'email': req.body.email,
            'roles': req.body.roles,
        });

        res.send({ //Send back valid response to the client
            valid: true,
            message: 'User: ' + result.username + ' was created'
        });
    }
    catch (error) {
        console.log(error)
        if (error.code == 11000) { //Send back in invalid response if username already exists
            res.send({
                valid: false,
                message: 'Username ' + error.keyValue.username + ' already exists'
            });
        }
    }

}

const deleted = async (req, res) => {

    const selected_id = req.body._id;
    const selected_user = req.body.username;

    try {
        // attempt to delete the user from the users collection
        const processResult = await User.findByIdAndRemove({ "_id": selected_id });
        if (processResult) {
            //Delete all associated groups the user controls
            await Group.deleteMany({ "admin": selected_id });
            res.send({
                "valid": true,
                'message': 'User ' + selected_user + ' has been deleted'
            });
        }
        else {
            res.send({
                'valid': false,
                'message': 'User ' + selected_user + ' does not exist'
            })
        }
    }
    catch (error) {
        console.log(error.name + ': ' + error.message);
        res.send({
            "valid": false,
            'message': 'Unknown error occured'
        });
    }
}


const getusers = async (req, res) => {

    const user_id = req.verified.credentials._id;

    try {
        //Get back a global list of all usernames and user_ids in the database, exlcluding the current user 
        const userList = await User.find({ "_id": { '$ne': user_id } }).select({ "username": 1, 'avatar': 1 });
        res.send(userList);
    }
    catch (error) {
        console.log(error.name + ': ' + error.message);
    }
}

module.exports = {
    create,
    deleted,
    getusers
}