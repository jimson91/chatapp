const Group = require('../models/Group');
const Channel = require('../models/Channel');

const create = async (req, res) => {

    const channel_name = req.body.newchannelname;
    const group_id = req.body.group_id;
    const user_id = req.verified.credentials._id;

    try {
        //Ensure the channel does not already exist from the same group
        const count = await Channel.countDocuments({ 'name': channel_name, 'group_id': group_id });

        if (count == 0) {
            // Create new instance of channel ensuring the user who sent the request is the first member to be added
            await Channel.create({ 'name': channel_name, 'group_id': group_id, 'members': [user_id], 'messages': [] });
            res.send({
                'valid': true,
                'message': 'Channel ' + channel_name + ' has been created'
            });
        }
        else {
            res.send({
                'valid': false,
                'message': 'Channel ' + channel_name + ' already exists'
            });
        }
    }
    catch (error) {
        console.log(error.name + ': ' + error.message);
        res.send({
            'valid': false,
            'message': 'Unknown error occured'
        });
    }
}

const deleted = async (req, res) => {
    
    const group_id = req.body.group_id;
    const user_id = req.verified.credentials._id;
    const channel_id = req.body.channel_id;

    try {
        //Ensure that the group exists and the user is an admin before deleting
        const count = await Group.countDocuments({ '_id': group_id, 'admin': user_id });
        //Ensure that the channel exists
        const exists = await Channel.countDocuments({ '_id': channel_id });
        if (count == 1 && exists == 1) {
            //Delete the channel that belongs to the recieved group_id
            await Channel.deleteOne({ '_id': channel_id, 'group_id': group_id });
            res.send({
                'valid': true,
                'message': 'Channel has been deleted'
            });
        }
        else {
            res.send({
                'valid': false,
                'message': 'Unauthorised Request or the channel does not exist'
            });
        }
    }
    catch (error) {
        console.log(error);
        res.send({
            'valid': false,
            'message': 'Unknown error occured'
        });
    }
}

module.exports = {
    create,
    deleted
}