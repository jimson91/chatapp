const fs = require('fs');
const User = require('../models/User');
const Channel = require('../models/Channel');
const formidable = require('formidable');
const path = require('path');

const imageupload = async (req, res) => {

    const user_id = req.verified.credentials._id;
    const lookup = await User.findById({ "_id": user_id }).select({ 'avatar': 1 }); //Lookup the current users avatar (if any exists)

    var form = new formidable.IncomingForm({ uploadDir: path.join(__dirname, '../userimages') });
    form.keepExtensions = true;
    form.parse(req);

    form.on('error', function (err) {
        res.send({
            result: "failed",
            data: {},
            numberOfImages: 0,
            message: "Cannot upload images. Error is: " + err
        });
        console.log(err);
    });


    form.on('fileBegin', function (name, file) {
        file.path = form.uploadDir + "/" + file.name; //Rename the incoming file to the files name
    });

    form.on('file', function (field, file) {
        try {
            if (lookup.avatar !== 'default.jpg' && lookup.avatar !== file.name) { //Remove the users older profile image from the file system
                fs.unlinkSync(form.uploadDir + "/" + lookup.avatar);
            }

            User.findByIdAndUpdate({ "_id": user_id }, { $set: { "avatar": file.name } }, (err) => { //Update the users avatar name
                if (err) {
                    console.log(err);
                }

                User.findById({ "_id": user_id }, { 'groups': 1 }, (error, user) => {

                    for (let i in user.groups) {
                        Channel.updateMany({
                            'group_id': user.groups[i], 'messages': {
                                $elemMatch: {
                                    "_id": user_id
                                }
                            }
                        },
                        {
                            $set: { 'messages.$.avatar': file.name }, 
                        }, (e,r) => {

                        });
                    }
                });

                //Send back a valid response
                res.send({
                    result: "OK",
                    data: { 'filename': file.name, 'size': file.size },
                    numberofImages: 1,
                    message: "upload sucessful"
                });

            });
        }
        catch (e) {
            console.log(e);
        }
    });

};

module.exports = {
    imageupload
}