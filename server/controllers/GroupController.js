const User = require('../models/User');
const Group = require('../models/Group');
const Channel = require('../models/Channel');

const create = async (req, res) => {

    const group = req.body.newGroupName;
    const user_id = req.verified.credentials._id;

    try {
        //Check if the group already exists and the user is an admin of group
        const count = await Group.countDocuments({ 'admin': user_id, 'name': group });
        if (count == 0) {

            //Add the new group to the groups collection
            const result = await Group.create({ 'name': group, 'admin': user_id, 'members': [user_id] });
            //Append the group to the users groups list
            await User.findByIdAndUpdate({ "_id": user_id }, { $push: { 'groups': result._id } });

            res.send({
                'valid': true,
                'message': group + ' was created'
            });
        }
        else {
            res.send({
                'valid': false,
                'message': group + ' already exists'
            });
        }
    } catch (error) {
        console.log(error.name + ': ' + error.message);
        res.send({
            'valid': false,
            'message': 'Unknown error occured'
        });
    }
}

const deleted = async (req, res) => {

    const user_id = req.verified.credentials._id;
    const group = req.body.group;
    const group_id = req.body.group_id;

    try {
        //Ensure that the group exists and the user is an admin of group before deleting
        const count = await Group.countDocuments({ 'admin': user_id, '_id': group_id });
        if (count == 1) {
            //Pull the group from every users group list
            await User.updateMany({}, { $pull: { 'groups': group_id } });
            //Delete the entire group entry from the groups collection
            await Group.findByIdAndRemove({ "_id": group_id });
            //Delete all associated channels
            await Channel.deleteMany({ "group_id": group_id });
            res.send({
                'valid': true,
                'message': group + ' was deleted'
            });
        }
        else {
            res.send({
                'valid': false,
                'message': group + ' does not exist'
            });
        }
    }
    catch (error) {
        console.log(error.name + ': ' + error.message);
        res.send({
            'valid': false,
            'message': 'Unknown error occured'
        });
    }
}


const admingroups = async (req, res) => {

    const user_id = req.verified.credentials._id;

    try {
        //Return an array of objects that contain all groups in which the user controls
        const admingroups = await Group.find({ "admin": user_id }).lean().select({ 'admin': 0 });
        //Find the members for each group the user controls
        for (let i in admingroups) {
            //Find the members associated with the group
            const members = await User.find({ '_id': { '$in': admingroups[i].members } }).select({ 'username': 1 });
            //Find the channels associated with the group
            const channel_lookup = await Channel.find({ 'group_id': admingroups[i]._id }).lean().select({ 'group_id': 0, 'messages': 0 });
            //Append the channels to the group object
            admingroups[i].channels = channel_lookup;
            //add the members array to the group object
            admingroups[i].members = members;
        }
        //Send back the admin group data
        res.send(admingroups);
    }
    catch (error) {
        console.log(error.name + ': ' + error.message);
    }
}

const grouplist = async (req, res) => {

    const user_id = req.verified.credentials._id;
    
    try {
        //Return an array of objects that contain all groups[id/name/admin] in which the user is a member
        const grouplist = await Group.find({ "members": user_id }).lean().select({ "members": 0 });
        for (let i in grouplist) {
            //For each group object the user belongs
            const channellist = await Channel.find({ "group_id": grouplist[i]._id }).select({ "_id": 1, "name": 1 });
            //Append an array of objects containing the channels[id/name] in which the user belongs
            grouplist[i].channels = channellist;
        }
        res.send(grouplist);
    }
    catch (error) {
        console.log(error.name + ': ' + error.message);
    }
}

module.exports = {
    create,
    deleted,
    admingroups,
    grouplist
}