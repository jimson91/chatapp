const User = require('../models/User');
const jwt = require('jsonwebtoken');

const authenticate = async (req, res) => {

    const username = req.body.username;
    const password = req.body.password;

    try {
        //Check if the username exists, if so retrieve user details
        credentials = await User.findOne({ 'username': username }).lean().select({ "groups": 0 });
        if (credentials) {
            // Compare recieved password hash against stored password hash using Bcrypt
            if (await User.validatePassword(password, credentials.password)) {
                //Remove password from user object before sending
                delete credentials.password;

                // specify token signing options
                const signOptions = {
                    issuer: 'ChatApp',
                    subject: credentials.email,
                    expiresIn: '3h',
                    algorithm: 'HS256',
                }
                // Sign the authentication data and send token back to client
                jwt.sign({ credentials }, 'secretkey', signOptions, (err, token) => {
                    credentials.access_token = token;
                    res.send(credentials);
                    // Log error if signing error occures
                    if (err) {
                        console.log(err);
                    }
                });
            }
            else {
                res.send({
                    valid: false,
                    message: "The password is incorrect"
                });
            }
        }
        else {
            res.send({
                valid: false,
                message: "The username does not exist"
            });
        }
    }
    catch (error) {
        console.log(error.name + ': ' + error.message);
        res.send({
            valid: false,
            message: "Unknown error occurred"
        });
    }
};

const authchange = async (req, res) => {

    const user_id = req.verified.credentials._id;
    const oldPwd = req.body.currentPassword;
    const newPwd = req.body.newPassword;

    try {
        credentials = await User.findOne({ '_id': user_id }).select({ "password": 1 });
        // authenticate user password before processing new password
        if (await User.validatePassword(oldPwd, credentials.password)) {
            const hashedpassword = User.generateHash(newPwd);
            await User.findByIdAndUpdate({ "_id": user_id }, { "password": hashedpassword });

            res.send({
                'valid': true,
                'message': 'Password update successful'
            });
        }
        else {
            res.send({
                'valid': false,
                'message': 'Incorrect password'
            });
        }
    }
    catch (error) {
        console.log(error.name + ': ' + error.message);
    }
}

module.exports = {
    authenticate,
    authchange
}