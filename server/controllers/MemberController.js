const User = require('../models/User');
const Group = require('../models/Group');

const addMember = async (req, res) => {

    const user_id = req.body.userid;
    const group_id = req.body.groupid;
    const username = req.body.username;
    const group = req.body.groupname;

    try {
        //Check if the user is already a member of the group
        const count = await User.countDocuments({ '_id': user_id, 'username': username, 'groups': group_id });
        if (count == 0) {
            //Append the members array with the users id for the specific group in the groups collection
            await Group.findByIdAndUpdate({ "_id": group_id }, { $push: { "members": user_id } });
            //Append the groups array with the group_id for the specific user in the users collection
            await User.findByIdAndUpdate({ '_id': user_id }, { $push: { 'groups': group_id } });
            //Once the user is added to the group then send back a valid response message
            res.send({
                'valid': true,
                'message': 'User ' + username + ' was added to ' + group
            });
        } else {
            //If the user is already a member then send back an invalid response message
            res.send({
                'valid': false,
                'message': 'User ' + username + ' is already a member of ' + group
            });
        }
    } catch (error) {
        console.log(error.name + ': ' + error.message);
        res.send({
            'valid': false,
            'message': 'Unknown error occured'
        });
    }
}

const removeMember = async (req, res) => {

    const member_id = req.body.memberid;
    const group_id = req.body.groupid;
    const username = req.body.membername;
    const groupname = req.body.groupname;

    try {
        //Check if the recieved user is a valid member of the group
        const count = await User.countDocuments({ '_id': member_id, 'username': username, 'groups': group_id });
        if (count == 1) {
            /*If user is a valid member of the group, 
            1. Pull the group_id from the users groups array
            2. Pull the user_id from the groups membes array*/
            await User.findByIdAndUpdate({ '_id': member_id }, { $pull: { 'groups': group_id } });
            await Group.findByIdAndUpdate({ '_id': group_id }, { $pull: { 'members': member_id } });
            //Once the user is removed from the group send a valid response
            res.send({
                'valid': true,
                'message': 'User ' + username + ' has been removed from ' + groupname
            });
        }
        else {
            //If the user does not belong to the group send back an invalid response
            res.send({
                'valid': false,
                'message': 'User ' + username + ' does not belong to ' + groupname
            });
        }
    }
    catch (error) {
        console.log(error.name + ': ' + error.message);
        res.send({
            'valid': false,
            'message': 'Unknown error occured'
        });
    }
}

module.exports = {
    addMember,
    removeMember
}