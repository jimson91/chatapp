const express = require('express');
const router = express.Router();

const SU_Grant = require('../middleware/SU-check');
const SGU_Grant = require('../middleware/SGU-check');
const verification = require('../middleware/verify-token');
const validateRules = require('../middleware/validate-rules');

const UserController = require('../controllers/UserController');

router.post(
    '/create',
    verification,
    validateRules.userCreate(),
    validateRules.validate, 
    UserController.create
);
router.post(
    '/delete',
    verification,
    SU_Grant,
    validateRules.userDelete(),
    validateRules.validate,
    UserController.deleted
);
router.get(
    '/list',
    verification,
    SGU_Grant,
    UserController.getusers
);

module.exports = router;