const express = require('express');
const router = express.Router();

const SGU_Grant = require('../middleware/SGU-check');
const verification = require('../middleware/verify-token');
const validateRules = require('../middleware/validate-rules');

const MemberController = require('../controllers/MemberController');

router.post(
    '/addmember',
    verification,
    SGU_Grant,
    validateRules.memberAdd(),
    validateRules.validate,
    MemberController.addMember
);

router.post(
    '/removemember',
    verification,
    SGU_Grant,
    validateRules.memberRemove(),
    validateRules.validate,
    MemberController.removeMember
);

module.exports = router;