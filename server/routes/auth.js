const express = require('express');
const router = express.Router();

const verification = require('../middleware/verify-token');
const validateRules = require('../middleware/validate-rules');

const AuthController = require('../controllers/AuthController');

router.post(
    '/login',
    validateRules.authenticate(),
    validateRules.validate,
    AuthController.authenticate
);

router.post(
    '/authchange',
    verification,
    validateRules.authChange(),
    validateRules.validate,
    AuthController.authchange
);

module.exports = router;