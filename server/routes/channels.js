const express = require('express');
const router = express.Router();

const SGU_Grant = require('../middleware/SGU-check');
const verification = require('../middleware/verify-token');
const validateRules = require('../middleware/validate-rules');

const ChannelController = require('../controllers/ChannelController');

router.post(
    '/create',
    verification,
    SGU_Grant,
    validateRules.channelCreate(),
    validateRules.validate,
    ChannelController.create
);

router.post(
    '/delete',
    verification,
    SGU_Grant,
    validateRules.channelDelete(),
    validateRules.validate,
    ChannelController.deleted
);

module.exports = router;