const express = require('express');
const router = express.Router();

const SGU_Grant = require('../middleware/SGU-check');
const verification = require('../middleware/verify-token');
const validateRules = require('../middleware/validate-rules');

const GroupController = require('../controllers/GroupController');

router.post(
    '/create',
    verification,
    SGU_Grant,
    validateRules.groupCreate(),
    validateRules.validate,
    GroupController.create
);

router.post(
    '/delete',
    verification,
    SGU_Grant,
    validateRules.groupDelete(),
    validateRules.validate,
    GroupController.deleted
);

router.get(
    '/adminlist', 
    verification, 
    SGU_Grant, 
    GroupController.admingroups
);

router.get(
    '/list', 
    verification, 
    GroupController.grouplist
);

module.exports = router;