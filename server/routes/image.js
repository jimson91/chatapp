const express = require('express');
const router = express.Router();

const verification = require('../middleware/verify-token');

const ImageController = require('../controllers/ImageController');

router.post('/upload', verification, ImageController.imageupload);

module.exports = router;