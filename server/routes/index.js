const express = require('express');
const router = express.Router();
const path = require('path');

router.get("*", function (req, res) {
    res.sendFile(path.join(__dirname, '../../dist/chatApp/', 'index.html'));
});

router.post("*", (req, res) => {
    res.send({ valid: false });
});

module.exports = router;