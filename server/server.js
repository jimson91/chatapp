const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http').Server(app);
const io = require('socket.io')(http);
const ip = require("ip");

process.env.HOST = ip.address();

const PORT = process.env.PORT || 8080; // set server port
const HOST = process.env.HOST //set local ip address

const logger = require('./debug/logger');

//Add logger to express server
app.use(logger);

//Add cross-origin resource sharing to express server
app.use(cors());

//Add bodyparser module to express server
app.use(bodyParser.json({ limit: '50mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.use(express.static(path.join(__dirname, '../dist/chatApp/')));
app.use('/images', express.static(path.join(__dirname, './userimages')));

//Connect the mongodb database
require('./database/mongo.js')(app, io);

//Start the server
require('./listen.js')(http, PORT, HOST);