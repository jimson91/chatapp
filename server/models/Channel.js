const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ObjectID = Schema.Types.ObjectId;

const channelSchema = new Schema({
    name: { 
        type: String, 
        required: true 
    },
    group_id: { 
        type: ObjectID, 
        ref: 'Group',
        required: true,
    },
    messages: [{ 
        avatar: String, 
        from: String, 
        body: String, 
        time: String 
    }]
});

module.exports = mongoose.model('Channel', channelSchema);