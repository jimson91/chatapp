const mongoose = require('mongoose');
const bcrypt = require("bcryptjs");

const Schema = mongoose.Schema;
const ObjectID = Schema.Types.ObjectId;

//Define the user schema
const userSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    avatar: {
        type: String,
        default: 'default.jpg',
        required: true
    },
    roles: {
        SuperAdmin: {
            type: Boolean,
            required: true
        },
        GroupAdmin: {
            type: Boolean,
            required: true
        }
    },
    groups: [{
        type: ObjectID,
        ref: 'Group',
        require: true
    }]
});

const saltRounds = 10;

// Hash user password before saving into database
userSchema.pre('save', function (next) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
    next();
});

userSchema.statics.generateHash = function(password) {
    return bcrypt.hashSync(password, saltRounds);
};
// Validate user password
userSchema.statics.validatePassword = async function (recieved_password, stored_password) {
    return await bcrypt.compare(recieved_password, stored_password);
}

module.exports = mongoose.model('User', userSchema);