const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ObjectID = Schema.Types.ObjectId;

const groupSchema = new Schema({
    name: { 
        type: String, 
        required: true 
    },
    admin: { 
        type: ObjectID, 
        ref: 'User',
        required: true,
    },
    members: [{ 
        type: ObjectID, 
        ref: 'User',
        required: true
    }]
});

module.exports = mongoose.model('Group', groupSchema);